<?php
namespace Drupal\nnlm_emergency_status;
class Utilities {
  private static $messages = array();
  public static function getEmergencyRecipients($rebuild = FALSE) {
    global $databases;
    $recipent_list = &drupal_static('nnlm_emergency_recipient_list');
    if (empty($recipent_list)) {
      $cache = cache_get('nnlm_emergency_recipient_list');
      if (isset($cache) && !empty($cache->data)) {
        $recipent_list = $cache->data;
      }
    }
    if (empty($recipent_list) || $rebuild) {
      $recipent_list = array();
      //build recipient list.
      $docline_db = $databases['docline']['default'];
      $general_db = $databases['general']['default'];
      $connection = new \PDO('mysql:host=' . $docline_db['host'] . ';dbname=' . $docline_db['database'], $docline_db['username'], $docline_db['password']);

      $query_string = " SELECT s.STATE_ETC_CODE as 'State', r.Email
        FROM general.regions r
        INNER JOIN docline.PUBLIC_STATE_ETC_CODES s
        ON (r.Region_Code = s.REGION_CODE)";
      $stmt = $connection->query($query_string, \PDO::FETCH_ASSOC);
      if ($stmt) {
        foreach ($stmt as $row) {
          $recipent_list[trim($row['State'])] = trim($row['Email']);
        }
        cache_set('nnlm_emergency_recipient_list', $recipent_list, 'cache');
      }
      else {
        throw new \Exception("Could not retrieve recipient list from docline");
      }
    }
    return $recipent_list;
  }
  /**
   * A better implementation of the drupal user_has_role function
   * @param  [type] $role_name [description]
   * @param  [type] $user      [description]
   * @return [type]            [description]
   */
  public static function user_has_role($role_name, $user = NULL) {
    if ($user == NULL) {
      global $user;
    }
    if (is_array($user->roles) && in_array($role_name, array_values($user->roles))) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Displays one of several preformatted error messages to the user
   *
   * @param  string $code the type of error that has occurred.  Valid
   *                      values are:
   *                        'no recipient'
   *
   * @return string       A string witht the formatted message.
   */
  public static function error($code = 'general') {
    if (empty(self::$messages)) {
      self::$messages = array(
        'general' => t("Unknown error (code $code)"),
        'no recipient' => t('no email recipient has been designated for this form.  Please contact your RML representative and let them know'),
        'no rml representative' => ('No RML representative could be determined for the provided state'),
      );
    }
    try {
      $msg = (isset(self::$messages[$code])) ? self::$messages[$code] : '';
      if (empty($code) || empty($msg)) {
        $msg = self::$messages['general'];
      }
      throw new \Exception($msg);
    }
    catch(\Exception$e) {
      drupal_set_message("An error has occurred in this application: " . $e->getMessage() . ".");
    }
  }
}

