#NN/LM Emergency Status Module

This module is provided to facilitate communication in the event of an NN/LM interruption in service.  It leverages a pre-existing webform on the NN/LM Drupal site as well as an external NN/LM database to provide routing of webform submission notifications.  It also provides a block that lives at the top left of the sidebar on all nnlm.gov home pages directing users to the webform whenever "Emergency mode" is set to active.

Module configuration can be found at: admin/config/system/nnlm/emergency
The default location for the webform is services/request.html.  The system has been set up to allow modifications to this default location, and the config panel will show whatever the current location is.  It does depend on the fact that the response webform is node 10149, and this module needs to be updated if that value ever changes.