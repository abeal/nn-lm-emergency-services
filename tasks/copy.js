module.exports = {
	build: {
		files: [{
			cwd: 'src',
			expand: true,
			src: [
				'**/*'
			],
			dest: 'build'
		}]
	}
};