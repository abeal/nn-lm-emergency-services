module.exports = {
    options: {
        args: ["--verbose"],
        exclude: [".git*","*.scss","node_modules"],
        compareMode: 'checksum',
        recursive: true
    },
    dev: {
        options: {
            src: "build/",
            dest: "/var/www/ws-sandy-dev.hsl.washington.edu/sites/all/modules/custom/nnlm_emergency_status",
            host: 'abeal@november.hsl.washington.edu',
            //dryRun:true,
            deleteAll: true // Careful this option could cause data loss, read the docs!
        }
    }
    //note: stage and prod are accomplished by git commits (CI server)
};
